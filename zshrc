# Path to your oh-my-zsh installation.
export ZSH=/Users/austinchang/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
#ZSH_THEME="robbyrussell"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugin_path="/Users/austinchang/.oh-my-zsh/custom/plugins"
plugins=(git)
source $plugin_path/opp/opp.zsh
source $plugin_path/opp/*.zsh

# User configuration

#export PATH="/usr/local/heroku/bin:/Users/austinchang/.nvm/v0.10.33/bin:/Users/austinchang/.rbenv/shims:/Users/austinchang/.rbenv/bin:/Users/austinchang/.pyenv/shims:/Applications/MacVim.app/Contents/MacOS:/Applications/Postgres.app/Contents/Versions/9.3/bin:/Users/austinchang/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin"
# export MANPATH="/usr/local/man:$MANPATH"

source $ZSH/oh-my-zsh.sh

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
 alias zshconfig="vim ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Custom

# autoload colors
autoload -U colors && colors

# Set custom prompt
#PROMPT='%n@%m:%~
#$ '
# Timestamp with color
#%{$fg[cyan]%}[%D{%H:%M:%S}]%{$reset_color%}
PROMPT='%{$fg[blue]%}%n@%m:%~%{$reset_color%}
%{$fg[green]%}$ %{$reset_color%}'

# Vi mode in zsh
bindkey -v

# autoload zmv (massive rename)
autoload -U zmv

# disable auto CD (typing directory name without CD)
unsetopt AUTO_CD

# Disable <esc-/> chord, behave like bash vi mode
vi-search-fix() {
  zle vi-cmd-mode
  zle .vi-history-search-backward
}
autoload vi-search-fix
zle -N vi-search-fix
bindkey -M viins '\e/' vi-search-fix

# Fix backspace key
bindkey "^?" backward-delete-char

# Behave like vi mode in bash
bindkey "^W" backward-kill-word
bindkey "^H" backward-delete-char
bindkey "^U" backward-kill-line

# n, shift-n to navigate Ctrl-R
bindkey "^R" history-incremental-search-backward

# correct shift-tab to go backwards
bindkey '^[[Z' reverse-menu-complete

