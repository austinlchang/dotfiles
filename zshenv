export PATH="/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin"

# add user bin to path
export PATH="$HOME/bin:$PATH"

# Add postgres to path
export PATH="/Applications/Postgres.app/Contents/Versions/9.4/bin:$PATH"

# Make terminal input start on new line
#export PS1="\u@\h:\w\n$ "

# set $EDITOR env variable
export EDITOR=vim
# add macvim binary to path
export PATH="/Applications/MacVim.app/Contents/MacOS:$PATH"
# have vi always call vim
alias vi='vim'
# Always have vi mode in Terminal #bash only?
#set -o vi

# Free up <c-s> for Ctrl.p plugin
stty -ixon -ixoff

# LS colors
export CLICOLOR=1
export LSCOLORS=ExFxCxDxCxegedabagacad


### GIT ALIASES
alias gs='git status'
# git diff with colors
alias gd='git diff -C -p --color=auto'
# git diff against the current head
alias gdh='gd --cached'
alias glp='git log -C -p --color=auto'
# show line changes of last commit
alias gc='git show --stat'
# Add all modified files ONLY
gamod() {
  git add `git status | grep modified | awk '{print $2}'`
}
# Git auto-complete (for bash only)
#if [ -f ~/.git-completion.bash  ]; then
  #. ~/.git-completion.bash
#fi

# python env autocompletion
if which pyenv > /dev/null; then eval "$(pyenv init -)"; fi

### GENERAL ALIASES
# Colorize the grep command output for ease of use (good for log files)##
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# make parent directories on demand
alias mkdir="mkdir -pv"

# Reload bash_profile
alias br='bash-reload'

alias ackrails='ack --ignore-dir={log,tmp}'

# List all kewl-commands
alias kc='kewl-commands'

# rbenv
export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"

# NVM node version manager
export NVM_DIR="/Users/austinchang/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm

# Heroku Toolbelt
export PATH="/usr/local/heroku/bin:$PATH"

# Get colors for manual pages
man() {
    env \
    LESS_TERMCAP_mb=$(printf "\e[1;31m") \
    LESS_TERMCAP_md=$(printf "\e[1;31m") \
    LESS_TERMCAP_me=$(printf "\e[0m") \
    LESS_TERMCAP_se=$(printf "\e[0m") \
    LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
    LESS_TERMCAP_ue=$(printf "\e[0m") \
    LESS_TERMCAP_us=$(printf "\e[1;32m") \
    man "$@"
}

# increase ulimit file-descriptors on mac osx
ulimit -n 1024
ulimit -u 1024
